package com.example.mailclient.controller;

import com.example.mailclient.model.DAOUser;
import com.example.mailclient.model.LivestreamEntity;
import com.example.mailclient.model.LivestreamPasswordRequest;
import com.example.mailclient.model.UserDTOUpdateRequest;
import com.example.mailclient.repository.LivestreamRepository;
import com.example.mailclient.repository.UserDAO;
import com.example.mailclient.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.json.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("livestreams")
public class LivestreamController {

    private final LivestreamRepository livestreamRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    public LivestreamController(LivestreamRepository livestreamRepository) {
        this.livestreamRepository = livestreamRepository;
    }

    @GetMapping(path = "public/getAllNonPrivateLive")
    public List<LivestreamEntity> findAllNonPrivateLive() {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 1, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllNonPrivatePlanned")
    public List<LivestreamEntity> findAllNonPrivatePlanned() {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 0, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllNonPrivateArchived")
    public List<LivestreamEntity> findAllNonPrivateArchived() {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 0, (byte) 1);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllLive")
    public List<LivestreamEntity> getAllLive() {
        final List<LivestreamEntity> temp = livestreamRepository.findByLiveAndExpired((byte) 1, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllPlanned")
    public List<LivestreamEntity> getAllPlanned() {
        final List<LivestreamEntity> temp = livestreamRepository.findByLiveAndExpired((byte) 0, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllArchived")
    public List<LivestreamEntity> getAllArchived() {
        final List<LivestreamEntity> temp = livestreamRepository.findByLiveAndExpired((byte) 0, (byte) 1);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    @GetMapping(path = "public/getAllNonPrivateLive/{username}")
    public List<LivestreamEntity> findAllNonPrivateLiveWithUserOnes(@PathVariable String username) {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 1, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        DAOUser user = userDAO.findByName(username);
        temp.addAll(user.getLivestreamEntities());
        return temp;
    }

    @GetMapping(path = "public/getAllNonPrivatePlanned/{username}")
    public List<LivestreamEntity> findAllNonPrivatePlannedWithUserOnes(@PathVariable String username) {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 0, (byte) 0);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        DAOUser user = userDAO.findByName(username);
        temp.addAll(user.getLivestreamEntities());
        return temp;
    }

    @GetMapping(path = "public/getAllNonPrivateArchived/{username}")
    public List<LivestreamEntity> findAllNonPrivateArchivedWithUserOnes(@PathVariable String username) {
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndLiveAndExpired((byte) 0, (byte) 0, (byte) 1);
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate).reversed());
        } catch (NullPointerException e) {
            // ignore
        }
        DAOUser user = userDAO.findByName(username);
        temp.addAll(user.getLivestreamEntities());
        return temp;
    }

    @GetMapping(path = "getAll")
    public List<LivestreamEntity> getAll() {
        final List<LivestreamEntity> temp = livestreamRepository.findAll();
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate));
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    // for unlocking them to the regular admin as global admin
    @GetMapping(path = "getAllHidden")
    public List<LivestreamEntity> getAllHidden() {
        final List<LivestreamEntity> temp = livestreamRepository.findByHidden(((byte) 1));
        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate));
        } catch (NullPointerException e) {
            // ignore
        }
        return temp;
    }

    // for unlocking them to the regular admin as global admin
    @GetMapping(path = "getAllNotAcquired/{username}")
    public List<LivestreamEntity> getAllNotAcquired(@PathVariable String username) {
        List<LivestreamEntity> result = new ArrayList<>();
        final List<LivestreamEntity> temp = livestreamRepository.findByHiddenAndExpired(((byte) 1), ((byte ) 0));

        try {
            temp.sort(Comparator.comparing(LivestreamEntity::getStartDate));
        } catch (NullPointerException e) {
            // ignore
        }
        DAOUser user = userDAO.findByName(username);
        Set<LivestreamEntity> usersStreams = user.getLivestreamEntities();
        for (LivestreamEntity stream : temp) {
            boolean found = false;
            for (LivestreamEntity usersStream : usersStreams) {
                if (stream.getId() == usersStream.getId()) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                result.add(stream);
            }
        }
        return result;
    }

    @PostMapping(path = "setPassword")
    public ResponseEntity<?> setPassword(@RequestBody LivestreamPasswordRequest livestreamPasswordRequest) {
        System.out.println(livestreamPasswordRequest);
        LivestreamEntity livestreamEntity = livestreamRepository.findById(livestreamPasswordRequest.getId());
        System.out.println("Found livestream: " + livestreamEntity.getDesc());
        livestreamEntity.setPassword(bcryptEncoder.encode(livestreamPasswordRequest.getPassword()));
        System.out.println("Going to save: " + livestreamEntity);
        livestreamRepository.save(livestreamEntity);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(path = "public/passwordExists/{id}")
    public ResponseEntity<?> passwordExists(@PathVariable int id) {
        System.out.println(id);
        final LivestreamEntity livestreamEntity = livestreamRepository.findById(id);
        if (livestreamEntity != null) {
            if (livestreamEntity.getPassword() != null && livestreamEntity.getPassword().length() > 0) {
                System.out.println("posielam 202");
                return new ResponseEntity(HttpStatus.ACCEPTED);
            } else {
                return new ResponseEntity(HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "public/checkPassword")
    public ResponseEntity<?> checkPassword(@RequestBody LivestreamPasswordRequest livestreamPasswordRequest) {
        final LivestreamEntity livestreamEntity = livestreamRepository.findById(livestreamPasswordRequest.getId());
        boolean matching = bcryptEncoder.matches(livestreamPasswordRequest.getPassword(), livestreamEntity.getPassword());
        if (matching){
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping(path = "public/getLinkOfVideo/{id}")
    public String getLinkOfVideo(@PathVariable int id) throws JSONException {
        final LivestreamEntity entity = livestreamRepository.findById(id);
        JSONObject jsonObject = new JSONObject(entity);
        return jsonObject.getString("link");
    }

    @PostMapping(value = "/update/admin")
    public ResponseEntity<?> updateGlobalAdmin(@RequestBody UserDTOUpdateRequest proposedAdminUser) throws
            Exception {
        return ResponseEntity.ok(userDetailsService.update(proposedAdminUser, proposedAdminUser.getExistingId()));
    }
}