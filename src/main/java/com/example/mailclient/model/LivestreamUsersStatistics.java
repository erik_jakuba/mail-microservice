package com.example.mailclient.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
public class LivestreamUsersStatistics implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String username;
    int livestreamId;
    private Timestamp timeOfJoin;
    private Timestamp timeOfLeave;

    public LivestreamUsersStatistics(){

    }

    public LivestreamUsersStatistics(String username, int livestreamId, Timestamp timeOfJoin, Timestamp timeOfLeave) {
        this.username = username;
        this.livestreamId = livestreamId;
        this.timeOfJoin = timeOfJoin;
        this.timeOfLeave = timeOfLeave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLivestreamId() {
        return livestreamId;
    }

    public void setLivestreamId(int livestreamId) {
        this.livestreamId = livestreamId;
    }

    public Timestamp getTimeOfJoin() {
        return timeOfJoin;
    }

    public void setTimeOfJoin(Timestamp timeOfJoin) {
        this.timeOfJoin = timeOfJoin;
    }

    public Timestamp getTimeOfLeave() {
        return timeOfLeave;
    }

    public void setTimeOfLeave(Timestamp timeOfLeave) {
        this.timeOfLeave = timeOfLeave;
    }
}
