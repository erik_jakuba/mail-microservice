package com.example.mailclient.controller;

import com.example.mailclient.config.JwtTokenUtil;
import com.example.mailclient.model.DAOUser;
import com.example.mailclient.model.LivestreamEntity;
import com.example.mailclient.model.SaveLivestreams;
import com.example.mailclient.model.UserDTO;
import com.example.mailclient.repository.LivestreamRepository;
import com.example.mailclient.repository.UserDAO;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "user")
public class UserController {

    @Autowired
    private final UserDAO userDAO;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private LivestreamRepository livestreamRepository;


    @Autowired
    private PasswordEncoder bcryptEncoder;

    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @PostMapping(path = "/save/livestreams")
    public ResponseEntity saveUser(@RequestBody SaveLivestreams saveLivestreams) {

        DAOUser user = userDAO.findByName(saveLivestreams.getUsername());
        System.out.println(user);
        if (user != null) {
            Set<LivestreamEntity> livestreamEntitySet = new HashSet<>();
            for (String livestream : saveLivestreams.getLivestreams()) {
                LivestreamEntity livestreamEntity = livestreamRepository.findByName(livestream).get(0);
                livestreamEntitySet.add(livestreamEntity);
            }
            if (livestreamEntitySet.size() == 0) {
                return ResponseEntity
                        .status(HttpStatus.NOT_ACCEPTABLE)
                        .body("No stream was found for: " + saveLivestreams.getLivestreams());
            }
            livestreamEntitySet.addAll(user.getLivestreamEntities());
            user.setLivestreamEntities(livestreamEntitySet);
            userDAO.save(user);
            return new ResponseEntity(HttpStatus.OK);
        }
        user = userDAO.findByName(saveLivestreams.getUsername());
        System.out.println(user);
        if (user != null) {
            Set<LivestreamEntity> livestreamEntitySet = new HashSet<>();
            for (String livestream : saveLivestreams.getLivestreams()) {
                LivestreamEntity livestreamEntity = livestreamRepository.findByName(livestream).get(0);
                livestreamEntitySet.add(livestreamEntity);
            }
            if (livestreamEntitySet.size() == 0) {
                return ResponseEntity
                        .status(HttpStatus.NOT_ACCEPTABLE)
                        .body("No stream was found for: " + saveLivestreams.getLivestreams());
            }
            livestreamEntitySet.addAll(user.getLivestreamEntities());
            user.setLivestreamEntities(livestreamEntitySet);
            userDAO.save(user);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping(path = "/delete/{adminName}")
    public ResponseEntity deleteUser(@PathVariable String adminName) {
        DAOUser user = userDAO.findByName(adminName);
        userDAO.delete(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/modify/{name}")
    public void updateUser(@PathVariable String name, @RequestBody UserDTO user) {
        DAOUser currentUser = userDAO.findByName(name);
        if (currentUser != null) {
            currentUser.setName(user.getUsername());
            currentUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            userDAO.save(currentUser);
        }
    }

    @GetMapping("/getAll")
    public List<DAOUser> getAll() {
        List<DAOUser> users = this.userDAO.findAll();
        users.forEach(user -> user.setPassword(null));
        return users;
    }

    @GetMapping("/getAllLivestresamsOfUser/{username}")
    public Set<LivestreamEntity> getAllLivestresamsOfUser(@PathVariable("username") String username) {
        DAOUser daoUser = this.userDAO.findByName(username);
        return daoUser.getLivestreamEntities();
    }
}
