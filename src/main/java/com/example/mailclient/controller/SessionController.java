package com.example.mailclient.controller;

import com.example.mailclient.model.LivestreamEntity;
import com.example.mailclient.model.Session;
import com.example.mailclient.repository.LivestreamRepository;
import com.example.mailclient.repository.SessionRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("sessions")
public class SessionController {

    private final SessionRepository sessionRepository;

    private final LivestreamRepository livestreamRepository;

    private String runtimeAdress = System.getenv("frontendLocation");

    @Autowired
    public SessionController(SessionRepository sessionRepository, LivestreamRepository livestreamRepository) {
        this.sessionRepository = sessionRepository;
        this.livestreamRepository = livestreamRepository;
    }

    @Autowired
    private JavaMailSender javaMailSender;

    //http://localhost:8081/#/stream/streamId=10/userId=20
    @PostMapping(path = "public/sendMail{id}")
    public void addUsers(@RequestBody String string, @PathVariable int id) throws JSONException {

        final LivestreamEntity entity = livestreamRepository.findById(id);

        final JSONObject jsonObject = new JSONObject(entity);
        final String title = jsonObject.getString("name");
        final String entityLink = jsonObject.getString("link");

        final String[] list = string.trim()
                .replace("%40", "@")
                .replace("=", "")
                .replace("%3B",";")
                .split("\\s*;\\s*");

        for (String mail : list) {
            if (sessionRepository.findByEmailAdress(mail) == null) {
                sessionRepository.save(new Session(mail));
            }
            final SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(mail);
            final Session session = sessionRepository.findByEmailAdress(mail);
            msg.setSubject("Invitation for " + title);
            if (runtimeAdress == null){
                runtimeAdress = "http://localhost:8081";
            }
            msg.setText("Let's connect for streaming on this link:" +  runtimeAdress + "/#/stream/streamId=" + id + "/sessionId=" + session.getUnique_id());
//            msg.setText("https://archiv.nti.sk/API/lplay.php?" + entityLink +  "/" + userRepository.findByEmailAdress(mail).getUnique_id());

            javaMailSender.send(msg);
        }
    }


    @GetMapping(path = "getSessions")
    public List<Session> getAll() {
        return sessionRepository.findAll();
    }

    @GetMapping(path = "/public/getSessionById/{sessionId}")
    public Session getSessionById(@PathVariable String sessionId) {
        System.out.println("SessionId= " + sessionId);
        Session session = sessionRepository.findByUniqueId(sessionId);
        System.out.println(session);
        return session;
    }

}
