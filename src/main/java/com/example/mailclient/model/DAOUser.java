package com.example.mailclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "user")
public class DAOUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "password")
    private String password;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    private Set<LivestreamEntity> livestreamEntities;

    private boolean globalAdmin = false;

    public DAOUser() {
    }

    @Override
    public String toString() {
        return "DAOUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", livestreamEntities=" + livestreamEntities +
                ", globalAdmin=" + globalAdmin +
                '}';
    }

    public DAOUser(@NotNull String name, @NotNull String password, Boolean globalAdmin) {
        this.name = name;
        this.password = password;
        this.globalAdmin = globalAdmin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<LivestreamEntity> getLivestreamEntities() {
        return livestreamEntities;
    }

    public void setLivestreamEntities(Set<LivestreamEntity> livestreamEntities) {
        this.livestreamEntities = livestreamEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGlobalAdmin() {
        return globalAdmin;
    }

    public void setGlobalAdmin(boolean globalAdmin) {
        this.globalAdmin = globalAdmin;
    }
}