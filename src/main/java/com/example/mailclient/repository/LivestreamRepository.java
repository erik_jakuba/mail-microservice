package com.example.mailclient.repository;

import com.example.mailclient.model.LivestreamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivestreamRepository extends JpaRepository<LivestreamEntity, Integer> {
   List<LivestreamEntity> findByHiddenAndLiveAndExpired(Byte hidden, Byte live, Byte expired);
   List<LivestreamEntity> findByHidden(Byte hidden);
   List<LivestreamEntity> findByHiddenAndExpired(Byte hidden, Byte expired);
   List<LivestreamEntity> findByLiveAndExpired(Byte live, Byte expired);
   LivestreamEntity findById(int id);
   List<LivestreamEntity> findByName(String name);
}