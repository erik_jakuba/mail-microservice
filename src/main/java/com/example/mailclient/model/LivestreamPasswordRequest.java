package com.example.mailclient.model;

import java.io.Serializable;

public class LivestreamPasswordRequest implements Serializable {
    private String password;
    private int id;

    public LivestreamPasswordRequest() {

    }

    public LivestreamPasswordRequest(String password, int id) {
        this.password = password;
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "LivestreamPasswordRequest{" +
                "password='" + password + '\'' +
                ", id=" + id +
                '}';
    }
}
